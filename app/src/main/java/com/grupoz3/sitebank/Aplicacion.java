package com.grupoz3.sitebank;

import android.app.Application;

import com.grupoz3.sitebank.Firebase.BancosAsinc;
import com.grupoz3.sitebank.Firebase.BancosFirestore;
import com.grupoz3.sitebank.datos.BancosBD;
import com.grupoz3.sitebank.datos.BancosLista;
import com.grupoz3.sitebank.datos.RepositoriosBancos;
import com.grupoz3.sitebank.modelo.GeoPunto;
import com.grupoz3.sitebank.presentacion.AdaptadorBancos;
import com.grupoz3.sitebank.presentacion.AdaptadorBancosBD;

public class Aplicacion extends Application {

    //public RepositoriosBancos bancos = new BancosLista();
    public BancosBD bancos;
    //public AdaptadorBancos adaptador = new AdaptadorBancos(bancos);
    //ADAPTADOR BASE DE DATOS SQLITE
    public AdaptadorBancosBD adaptador;

    //AÑADIENDO LOCALIZACIÓN EN MIS BANCOS
    public GeoPunto posicionActual = new GeoPunto(0.0, 0.0);

    //FIREBASE
    public BancosAsinc bancosAsinc;

    @Override
    public void onCreate() {
        super.onCreate();
        bancos = new BancosBD(this);
        adaptador= new AdaptadorBancosBD(bancos, bancos.extraeCursor());
        bancosAsinc = new BancosFirestore();
    }

    //get de RepositorioBancos
    public RepositoriosBancos getBancos() {
        return bancos;
    }
}