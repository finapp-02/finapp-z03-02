package com.grupoz3.sitebank.presentacion;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.grupoz3.sitebank.Aplicacion;
import com.grupoz3.sitebank.Firebase.AdaptadorBancosFirestore;
import com.grupoz3.sitebank.Firebase.BancosAsinc;
import com.grupoz3.sitebank.R;
import com.grupoz3.sitebank.datos.BancosBD;
import com.grupoz3.sitebank.datos.RepositoriosBancos;
import com.grupoz3.sitebank.modelo.Bancos;
import com.grupoz3.sitebank.casos_uso.CasosUsoBancos;
import com.squareup.picasso.Picasso;


public class VistaBancosActivity extends AppCompatActivity {

    //private RepositoriosBancos bancos;
    private CasosUsoBancos usoBancos;
    private int pos, _id = -1;
    private Bancos banco;
    private TextView nombre, tipo, direccion, telefono, url, comentario, horario, hora;
    private ImageView foto, galeria, camara, logo_tipo, eliminarFoto;
    private RatingBar valoracion;
    final static int RESULTADO_EDITAR = 1;
    final static int RESULTADO_GALERIA = 2;
    final static int RESULTADO_FOTO = 3;
    private Uri uriUltimaFoto;
    //PERMISO GALERIA READ_EXTERNAL_STORAGE
    private static final int SOLICITUD_PERMISO_LECTURA = 0;
    //BASE DE DATOS SQLITE
    private BancosBD bancos;
    private AdaptadorBancosBD adaptador;

    //FIRESTORE
    private AdaptadorBancosFirestore adaptadorBancosFirestore;
    private String idBancos;
    private CollectionReference instanciaVista = FirebaseFirestore.getInstance().collection("bancos");
    private BancosAsinc bancosAsinc;
    private int bancoRecurso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vista_bancos);

        bancos = ((Aplicacion) getApplication()).bancos;
        //usoBancos = new CasosUsoBancos(this, bancos);
        adaptador= ((Aplicacion)getApplication()).adaptador;
        //usoBancos = new CasosUsoBancos(this, bancos, adaptador);
        usoBancos = new CasosUsoBancos(this, bancosAsinc, adaptadorBancosFirestore);
        bancosAsinc = ((Aplicacion) getApplication()).bancosAsinc;
        //banco = bancos.elemento(pos);

        Query query = FirebaseFirestore.getInstance().collection("bancos").limit(50);
        FirestoreRecyclerOptions<Bancos> opciones = new FirestoreRecyclerOptions.Builder<Bancos>().setQuery(query, Bancos.class).build();
        adaptadorBancosFirestore = new AdaptadorBancosFirestore(opciones, this);

        Bundle extras = getIntent().getExtras();
        idBancos = extras.getString("bancos_fire");
        bancoRecurso = extras.getInt("icono_recurso");
        Log.d("TAG VBA","vista banco elegido-> " + idBancos + " coleccion " + FirebaseFirestore.getInstance().collection("bancos").document(idBancos));
        Log.d("TAG VBA","vista banco elegido-> " + idBancos + " coleccion "+ instanciaVista.document(idBancos) + " banco recurso " + bancoRecurso);
        pos = extras.getInt("pos");
        Log.d("TAG VBA", "oncreate vba " + pos + "id " + _id);
        datosBancosFirestore();
        adaptadorBancosFirestore.startListening();
        /*
        if(extras != null) pos = extras.getInt("pos", 0);
        else pos=0;
        _id = adaptador.idPosicion(pos);

        banco = adaptador.bancosPosicion(pos);
        */
        foto = findViewById(R.id.foto);
        //galeria = findViewById(R.id.galeria);
        //camara = findViewById(R.id.camara);
        //eliminarFoto = findViewById(R.id.eliminar);

        actualizaVistas();
        llamar();
        verWeb();
        //abrirGaleria();
        //tomarFotoCamara();
        //eliminarFoto();
    }

    private void datosBancosFirestore() {
        instanciaVista.document(idBancos).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(Task<DocumentSnapshot> task) {
                String name = task.getResult().getString("nombre");
                nombre.setText(name);
                String comentario_ = task.getResult().getString("comentario");
                comentario.setText(comentario_);
                String direccion_ = task.getResult().getString("direccion");
                direccion.setText(direccion_);
                Long phone = task.getResult().getLong("telefono");
                telefono.setText(String.valueOf(phone));
                String type = task.getResult().getString("tipo");
                tipo.setText(type);
                String la_url = task.getResult().getString("url");
                url.setText(la_url);
                String horario_dia = task.getResult().getString("horario");
                horario.setText(horario_dia);
                String hora_ = task.getResult().getString("hora");
                hora.setText(hora_);
                String photo = task.getResult().getString("foto");

                if (photo == null || photo == "") foto.setImageResource(bancoRecurso);
                else {
                    Picasso.get().load(photo).error(R.drawable.iconoapp_azul).placeholder(R.drawable.otros).into(foto);
                    //se puede cambiar el fondo que puso el profesor ic_launcher_background y en R.drawable.otros podemos remplazar el
                    //otros con nuestro logo de la app
                    Log.d("TAG","imagen " + Uri.parse(photo));
                    //foto.setImageURI(Uri.parse(photo));
                }//ImageResource(photo);

                logo_tipo.setImageResource(bancoRecurso); //item_bancos.getTipo().getRecurso());
                Double valor = task.getResult().getDouble("valoracion");
                valoracion.setRating(valor.floatValue());
            }
        });
    }

    public void actualizaVistas() {

        nombre = findViewById(R.id.nombre);
        //nombre.setText(banco.getNombre());

        logo_tipo = findViewById(R.id.logo_tipo);
        //logo_tipo.setImageResource(banco.getTipo().getRecurso());

        tipo = findViewById(R.id.tipo);
        //tipo.setText(banco.getTipo().getTexto());

        direccion = findViewById(R.id.direccion);
        //direccion.setText(banco.getDireccion());

        telefono = findViewById(R.id.telefono);
        //telefono.setText(Long.toString(banco.getTelefono()));

        url = findViewById(R.id.url);
        //url.setText(banco.getUrl());

        comentario = findViewById(R.id.comentario);
        //comentario.setText(banco.getComentario());

        horario = findViewById(R.id.horario);
        //horario.setText(banco.getHorario());

        hora = findViewById(R.id.hora);
        //hora.setText(banco.getHora());

        valoracion = findViewById(R.id.valoracion);
        /*valoracion.setRating(banco.getValoracion());
        valoracion.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener(){
            @Override
            public void onRatingChanged(RatingBar ratingBar, float valor, boolean fromUser) {
                banco.setValoracion(valor);
                usoBancos.actualizaPosBancos(pos,banco);
                pos = adaptador.posicionId(_id);

            }
        });

        usoBancos.visualizarFoto(banco, foto);*/
    }

    public void llamar(){
        telefono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //usoBancos.llamarTelefono(banco);
                instanciaVista.document(idBancos).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot documentSnapshot = task.getResult();
                            if (documentSnapshot.exists()) {
                                Log.d("TAG VBA", "Ver datos telefono " + documentSnapshot.getData() + " id " + documentSnapshot.getId());
                                Double telefono = task.getResult().getDouble("telefono");
                                Integer phone = telefono.intValue();
                                Log.d("VER", "telefono " + Uri.parse("tel: " + phone));
                                Intent ver = new Intent(Intent.ACTION_VIEW, Uri.parse("tel: " + phone));
                                startActivity(ver);
                            } else {
                                Log.d("TAG VBA", "No such document");
                            }
                        } else {
                            Log.d("TAG VBA", "get failed with ", task.getException());
                        }
                    }
                });
            }
        });
    }

    public void verWeb() {
        url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //usoBancos.verWeb(banco);
                instanciaVista.document(idBancos).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot documentSnapshot = task.getResult();
                            if (documentSnapshot.exists()) {
                                Log.d("TAG VBA", "ver datos compartir " + documentSnapshot.getData() + " id " + documentSnapshot.getId());
                                String url = task.getResult().getString("url");
                                Uri web = Uri.parse(url);
                                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                                    web = Uri.parse("http://" + url);
                                    Log.d("VER", "url 1 " + web);
                                }
                                Log.d("VER", "url " + web);
                                Intent ver = new Intent(Intent.ACTION_VIEW, web); //Uri.parse(Uri.encode(url)));
                                startActivity(ver);
                            } else {
                                Log.d("TAG", "No such document");
                            }
                        } else {
                            Log.d("TAG", "get failed with ", task.getException());
                        }
                    }
                });
            }
        });
    }

    public void abrirGaleria() {
        galeria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usoBancos.ponerDeGaleria(RESULTADO_GALERIA);
            }
        });
    }

    public void tomarFotoCamara(){
        camara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uriUltimaFoto = usoBancos.tomarFoto(RESULTADO_FOTO);
            }
        });
    }

    public void eliminarFoto(){
        eliminarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usoBancos.eliminar_foto(pos, foto);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULTADO_EDITAR) {
            //banco = bancos.elemento(_id);
            //pos = adaptador.posicionId(_id);
            pos = adaptadorBancosFirestore.getPos(idBancos);
            banco = adaptadorBancosFirestore.getItem(_id);
            actualizaVistas();
            findViewById(R.id.scrollView1).invalidate();
        } else if (requestCode == RESULTADO_GALERIA) {
            if (resultCode == Activity.RESULT_OK) {
                usoBancos.ponerFoto(pos, data.getDataString(), foto);
            } else {
                Toast.makeText(this, R.string.msnfoto, Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == RESULTADO_FOTO) {
            if (resultCode == Activity.RESULT_OK && uriUltimaFoto != null) {
                banco.setFoto(uriUltimaFoto.toString());
                usoBancos.ponerFoto(pos, banco.getFoto(), foto);
            } else {
                Toast.makeText(this, R.string.errorcaptura, Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.vista_bancos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_compartir:
                //usoBancos.compartir(banco);
                instanciaVista.document(idBancos).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot documentSnapshot = task.getResult();
                            if (documentSnapshot.exists()) {
                                Log.d("TAG VBA", "ver datos compartir " + documentSnapshot.getData() + " id " + documentSnapshot.getId());
                                String name = task.getResult().getString("nombre");
                                String url = task.getResult().getString("url");
                                String direccion = task.getResult().getString("direccion");
                                String horario = task.getResult().getString("horario");
                                String hora = task.getResult().getString("hora");
                                Long telefono = task.getResult().getLong("telefono");
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("text/plain");
                                i.putExtra(Intent.EXTRA_TEXT, getString(R.string.msjshare) + name + "\n" + getString(R.string.direccion) + direccion + "\n" + getString(R.string.horarioatencion)
                                + horario + " - " + hora + "\n" + getString(R.string.phonenumer) + telefono + "\n" + getString(R.string.website) + url);
                                startActivity(i);
                            } else {
                                Log.d("TAG", "No such document");
                            }
                        } else {
                            Log.d("TAG", "get failed with ", task.getException());
                        }
                    }
                });
                return true;
            case R.id.accion_llegar:
                //usoBancos.verMapa(banco);
                instanciaVista.document(idBancos).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()) {
                            DocumentSnapshot documentSnapshot = task.getResult();
                            if(documentSnapshot.exists()) {
                                Log.d("TAG","ver mapa datos " + documentSnapshot.getData() + " id " + documentSnapshot.getId());
                                String direccion_mapa = task.getResult().getString("direccion");
                                Uri uri = Uri.parse("geo:0,0?z=18&q=" + Uri.encode(direccion_mapa));
                                Log.d("TAG VBA","vermapa " + uri + " " + Uri.encode(direccion_mapa));
                                startActivity(new Intent("android.intent.action.VIEW", uri));
                            } else {
                                Log.d("TAG", "No such document");
                            }
                        } else {
                            Log.d("TAG", "get failed with ", task.getException());
                        }
                    }
                });
                return true;
            /*case R.id.accion_editar:
                //usoBancos.editar(pos, RESULTADO_EDITAR);
                return true;*/
            case R.id.accion_borrar:
                //int id = adaptador.idPosicion(pos);
                String id = adaptadorBancosFirestore.getKey(pos); //idPosicion(pos);
                Log.d("TAG VBA", "borrar " + id);
                usoBancos.borrar(id);
                //usoBancos.borrar(pos);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == SOLICITUD_PERMISO_LECTURA) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                usoBancos.ponerFoto(pos, banco.getFoto(), foto);
            } else {
                usoBancos.ponerFoto(pos, "", foto);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        adaptadorBancosFirestore.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adaptadorBancosFirestore.stopListening();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adaptadorBancosFirestore.stopListening();
    }
}