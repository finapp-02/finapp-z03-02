package com.grupoz3.sitebank.presentacion;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.grupoz3.sitebank.Aplicacion;
import com.grupoz3.sitebank.R;
import com.grupoz3.sitebank.datos.RepositoriosBancos;
import com.grupoz3.sitebank.modelo.Bancos;
import com.grupoz3.sitebank.modelo.GeoPunto;

public class AdaptadorBancos extends RecyclerView.Adapter<AdaptadorBancos.ViewHolder> {

    protected RepositoriosBancos bancos;  //Lista de lugares a mostrar
    protected View.OnClickListener onClickListener;

    //Constructor
    public AdaptadorBancos(RepositoriosBancos bancos) {
        this.bancos = bancos;
    }

    //Nuestro ViewHolder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nombre, direccion, distancia;
        public ImageView foto;
        public RatingBar valoracion;

        public ViewHolder(View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.nombre);
            direccion = itemView.findViewById(R.id.direccion);
            foto = itemView.findViewById(R.id.foto);
            valoracion = itemView.findViewById(R.id.valoracion);
            distancia = itemView.findViewById(R.id.distancia);
        }

        //Se personaliza un ViewHolder a partir de un tipo de banco
        public void personaliza(Bancos bancos) {
            nombre.setText(bancos.getNombre());
            direccion.setText(bancos.getDireccion());
            int id = R.drawable.otros;
            switch (bancos.getTipo()) {
                case BANCOS: id = R.drawable.banco; break;
                case CAJERO: id = R.drawable.cajero; break;
                case CORRESPONSAL: id = R.drawable.corresponsal; break;
            }
            foto.setImageResource(id);
            foto.setScaleType(ImageView.ScaleType.FIT_END);
            valoracion.setRating(bancos.getValoracion());

            GeoPunto pos = ((Aplicacion) itemView.getContext().getApplicationContext()).posicionActual;
            if (pos.equals(GeoPunto.SIN_POSICION) || bancos.getPosicion().equals(GeoPunto.SIN_POSICION)) {
                distancia.setText("... Km");
            } else {
                int d = (int) pos.distancia(bancos.getPosicion());
                if (d < 2000) {
                    distancia.setText(d + " m");
                } else{
                    distancia.setText(d / 1000 + " Km");
                }
            }
        }
    }

    //set de onClick
    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    //métodos propios del ViewHolder
    //ViewHolder con la vista de un elemento sin personalizar
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vistaElemento = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.elemento_lista, parent, false);
        vistaElemento.setOnClickListener(onClickListener);
        return new ViewHolder(vistaElemento);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Bancos banco = bancos.elemento(position);
        holder.personaliza(banco);
    }

    @Override
    public int getItemCount() {
        return bancos.tamaño();
    }
}