package com.grupoz3.sitebank.presentacion;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.grupoz3.sitebank.Aplicacion;
import com.grupoz3.sitebank.Firebase.AdaptadorBancosFirestore;
import com.grupoz3.sitebank.Firebase.BancosAsinc;
import com.grupoz3.sitebank.R;
import com.grupoz3.sitebank.casos_uso.CasosUsoActividades;
import com.grupoz3.sitebank.casos_uso.CasosUsoBancos;
import com.grupoz3.sitebank.casos_uso.CasosUsoLocalizacion;
import com.grupoz3.sitebank.datos.BancosBD;
import com.grupoz3.sitebank.datos.BancosLista;
import com.grupoz3.sitebank.datos.RepositoriosBancos;
import com.grupoz3.sitebank.modelo.Bancos;

public class MainActivity extends AppCompatActivity {

    //private RepositoriosBancos bancos;
    private CasosUsoBancos usoBancos;
    private CasosUsoActividades usoActividades;

    static final int RESULTADO_PREFERENCIAS = 0;

    private RecyclerView recyclerView;
    //private AdaptadorBancos adaptador;

    //AÑADIENDO LOCALIZACION EN MIS LUGARES
    private static final int SOLICITUD_PERMISO_LOCALIZACION = 1;
    private CasosUsoLocalizacion usoLocalizacion;

    //base de datos sqlite
    private AdaptadorBancosBD adaptador;
    private BancosBD bancos;

    //ADAPTADOR DE FIRESTORE
    private AdaptadorBancosFirestore adaptadorBancosFirestore;
    private BancosAsinc bancosAsinc;
    private CollectionReference instanciaMain = FirebaseFirestore.getInstance().collection("bancos");

    private static MainActivity currentcontext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        Log.d("tag", "on create main");

        bancos = ((Aplicacion) getApplication()).bancos;
        //usoBancos = new CasosUsoBancos(this, bancos);
        adaptador = ((Aplicacion) getApplication()).adaptador;
        bancosAsinc = ((Aplicacion) getApplication()).bancosAsinc;

        usoActividades = new CasosUsoActividades(this);
        //usoBancos = new CasosUsoBancos(this, bancos, adaptador);
        usoBancos = new CasosUsoBancos(this, bancosAsinc, adaptadorBancosFirestore);
        usoLocalizacion = new CasosUsoLocalizacion(this, SOLICITUD_PERMISO_LOCALIZACION);
        /**
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adaptador);

        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int posicion = recyclerView.getChildAdapterPosition(v);
                int posicion = (Integer) v.getTag();
                Log.d("TAG main","posicion adaptador" +posicion);
                usoBancos.mostrar(posicion);
            }
        });
        **/
        //barra de acciones
        Toolbar toolbar = findViewById(R.id.toolbar_Main);
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolbarLayout = findViewById(R.id.toolbar_layout_Main);
        toolbar.setTitle(getTitle());
        //Boton flotante FAB circular
        /**/
        FloatingActionButton fab = findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, R.string.mensaje_fab, Snackbar.LENGTH_LONG).setAction("Accion",null).show();
                //usoBancos.nuevo();
                Intent nuevo_lugar = new Intent(getApplicationContext(), EdicionBancosActivity.class);
                nuevo_lugar.putExtra("_id", "UID");
                startActivity(nuevo_lugar);
            }
        });
        /**
        //importar BancosBD a cloud firestore
        FirebaseFirestore firestoreDB_bancos = FirebaseFirestore.getInstance();
        for(int id = 0; id < adaptador.getItemCount(); id++) {
            Log.d("MAIN","tamaño base datos ->" + adaptador.bancosPosicion(id));
            firestoreDB_bancos.collection("bancos").add(adaptador.bancosPosicion(id));
        }
         **/

        //firestore base de datos en la nube
        cargarInfoFromFirestore();
        adaptadorBancosFirestore.startListening();
    }

    //consulta a firestore
    public void cargarInfoFromFirestore() {
        Query query = FirebaseFirestore.getInstance().collection("bancos").limit(50);
        FirestoreRecyclerOptions<Bancos> opciones = new FirestoreRecyclerOptions.Builder<Bancos>()
                .setQuery(query, Bancos.class).build();
        adaptadorBancosFirestore = new AdaptadorBancosFirestore(opciones, this);
        Log.d("TAG MAIN","cargar firestore " + query.toString() + "\nrecycler" + opciones.toString());
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adaptadorBancosFirestore);

        adaptadorBancosFirestore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int posicion = recyclerView.getChildAdapterPosition(v);
                Bancos item_bancos = adaptadorBancosFirestore.getItem(posicion);
                String id_bancos = adaptadorBancosFirestore.getSnapshots().getSnapshot(posicion).getId();
                Log.d("TAG MAIN", "Clic adaptador id " + id_bancos + " posicion " + posicion + " itembancos " + item_bancos.getTipo().getRecurso());
                Log.d("TAG MAIN", "doc seleccionado " + id_bancos + " coleccion\n" +
                        FirebaseFirestore.getInstance().collection("bancos").document(id_bancos));

                Context context = getAppContext();
                Intent intent = new Intent(context, VistaBancosActivity.class);
                intent.putExtra("bancos_fire", id_bancos);
                intent.putExtra("pos", posicion);
                intent.putExtra("icono_recurso", item_bancos.getTipo().getRecurso());
                context.startActivity(intent);
            }
        });
    }

    public static MainActivity getCurrentContext() {
        return currentcontext;
    }

    public static Context getAppContext() {
        return MainActivity.getCurrentContext();
    }

    //mostrar menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.ajustes) {
            usoActividades.lanzarPreferencias(RESULTADO_PREFERENCIAS);
            Log.d("Tag en Main", "Clic en la opcion ajustes");
            return true;
        }
        if (id == R.id.acercaDe){
            usoActividades.lanzarAcercaDe();
            return true;
        }
        /*if (id == R.id.menu_buscar) {
            lanzarVistaBancos(null);
            Log.d("Tag en Main", "Clic en la opcion buscar");
            return true;
        }*/
        if (id == R.id.menu_usuario) {
            usoActividades.lanzarUsuario();
            return true;
        }
        if (id == R.id.menu_mapa){
            usoActividades.lanzarMapa();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void lanzarVistaBancos(View view) {
        final EditText entrada = new EditText(this);
        entrada.setText("0");
        entrada.setInputType(InputType.TYPE_CLASS_NUMBER);

        new AlertDialog.Builder(this)
                .setTitle(R.string.seleccionbanco)
                .setMessage(R.string.indicaid)
                .setView(entrada)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //int id = Integer.parseInt(entrada.getText().toString());
                        String id = (entrada.getText().toString());
                        Log.d("TAG MAIN", "buscar");
                        usoBancos.mostrar(id);
                    }
                })
                .setNegativeButton((R.string.cancelar), null)
                .show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("tag", "on pause main");
        usoLocalizacion.desactivar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("tag", "on resume main");
        usoLocalizacion.activar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("tag", "on start main");
        adaptadorBancosFirestore.startListening();
        currentcontext = this;
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("tag", "on stop main");
        adaptadorBancosFirestore.stopListening();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("tag", "on destroy main");
        adaptadorBancosFirestore.stopListening();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == SOLICITUD_PERMISO_LOCALIZACION && grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            usoLocalizacion.permisoConcedido();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*if (requestCode == RESULTADO_PREFERENCIAS) {
            adaptador.setCursor(bancos.extraeCursor());
            adaptador.notifyDataSetChanged();
        }*/
    }
}