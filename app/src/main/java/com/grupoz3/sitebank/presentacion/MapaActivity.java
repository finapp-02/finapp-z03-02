package com.grupoz3.sitebank.presentacion;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.grupoz3.sitebank.Aplicacion;
import com.grupoz3.sitebank.R;
import com.grupoz3.sitebank.datos.RepositoriosBancos;
import com.grupoz3.sitebank.modelo.Bancos;
import com.grupoz3.sitebank.modelo.GeoPunto;

public class MapaActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mapa;
    //private RepositoriosBancos bancos;
    //base de datos sqlite
    private AdaptadorBancosBD adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapa);
        //bancos = ((Aplicacion) getApplication()).bancos;
        adaptador = ((Aplicacion) getApplication()).adaptador;
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mapa = googleMap;
        mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mapa.setMyLocationEnabled(true);
            mapa.getUiSettings().setZoomControlsEnabled(true);
            mapa.getUiSettings().setCompassEnabled(true);
        }

        if (adaptador.getItemCount() > 0) {//bancos.tamaño() > 0
            GeoPunto p = adaptador.bancosPosicion(0).getPosicion(); //bancos.elemento(0).getPosicion();
            mapa.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(p.getLatitud(), p.getLongitud()), 14));
            //se puede cambiar el zoom, minimo 2 y max 21
        }

        for (int n = 0; n < adaptador.getItemCount(); n++) {//bancos.tamaño()
            Bancos banco = adaptador.bancosPosicion(n); //bancos.elemento(n);
            GeoPunto p = banco.getPosicion();
            if (p != null && p.getLatitud() != 0) {
                Bitmap iGrande = BitmapFactory.decodeResource(getResources(), banco.getTipo().getRecurso());
                Bitmap icono = Bitmap.createScaledBitmap(iGrande, iGrande.getWidth() / 10, iGrande.getHeight() / 10, false);
                //se puede cambiar el tamaño de los iconos, cambiando el valor de 7 por el que se esta dividiendo
                mapa.addMarker(new MarkerOptions()
                        .position(new LatLng(p.getLatitud(), p.getLongitud()))
                        .title(banco.getNombre())
                        .snippet(banco.getDireccion()) //info que se puede cambiar
                        .icon(BitmapDescriptorFactory.fromBitmap(icono))); //icono del tipo de banco
            }
        }
        mapa.setOnInfoWindowClickListener(this);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        for (int pos = 0; pos < adaptador.getItemCount(); pos++) {//bancos.tamaño()
            //bancos.elemento(pos).getNombre()
            if (adaptador.bancosPosicion(pos).getNombre().equals(marker.getTitle())) {
                /*Intent intent = new Intent(this, VistaBancosActivity.class);
                intent.putExtra("pos", pos);
                startActivity(intent);
                break;*/
            }
        }
    }
}