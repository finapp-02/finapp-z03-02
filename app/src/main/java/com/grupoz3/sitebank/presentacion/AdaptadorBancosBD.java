package com.grupoz3.sitebank.presentacion;

import android.database.Cursor;

import com.grupoz3.sitebank.datos.BancosBD;
import com.grupoz3.sitebank.datos.RepositoriosBancos;
import com.grupoz3.sitebank.modelo.Bancos;

public class AdaptadorBancosBD extends AdaptadorBancos {

    protected Cursor cursor;

    //CONSTRUCTOR
    public AdaptadorBancosBD(RepositoriosBancos bancos, Cursor cursor) {
        super(bancos);
        this.cursor = cursor;
    }

    //MÉTODOS GET Y SET DE CURSOR
    public Cursor getCursor() {
        return cursor;
    }

    public void setCursor(Cursor cursor) {
        this.cursor = cursor;
    }

    public Bancos bancosPosicion(int posicion) {
        cursor.moveToPosition(posicion);
        return BancosBD.extraeBancos(cursor);
    }

    public int idPosicion(int posicion) {
        cursor.moveToPosition(posicion);
        if (cursor.getCount() > 0) {
            return cursor.getInt(0);
        } else {
            return -1;
        }
    }

    public int posicionId(int id) {
        int pos = 0;
        while (pos < getItemCount() && idPosicion(pos) != id) pos++;
        if (pos >= getItemCount()) {
            return -1;
        } else {
            return pos;
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int posicion) {
        //super.onBindViewHolder(holder, posicion);
        Bancos bancos = bancosPosicion(posicion);
        holder.personaliza(bancos);
        holder.itemView.setTag(new Integer(posicion));
    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }
}