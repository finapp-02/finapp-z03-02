package com.grupoz3.sitebank.presentacion;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.grupoz3.sitebank.Aplicacion;
import com.grupoz3.sitebank.Firebase.AdaptadorBancosFirestore;
import com.grupoz3.sitebank.Firebase.BancosAsinc;
import com.grupoz3.sitebank.R;
import com.grupoz3.sitebank.casos_uso.CasosUsoBancos;
import com.grupoz3.sitebank.datos.BancosBD;
import com.grupoz3.sitebank.datos.RepositoriosBancos;
import com.grupoz3.sitebank.modelo.Bancos;
import com.grupoz3.sitebank.modelo.GeoPunto;
import com.grupoz3.sitebank.modelo.TipoBancos;

public class EdicionBancosActivity extends AppCompatActivity {

    //private RepositoriosBancos bancos;
    private CasosUsoBancos usoBancos;
    private int pos;
    private String _id;
    private Bancos banco;
    private EditText nombre;
    private Spinner tipo;
    private EditText direccion;
    private EditText telefono;
    private EditText url;
    private EditText horario;
    private EditText hora;
    private EditText comentario;
    private Toast msnToast;
    //base de datos sqlite
    private BancosBD bancos;
    private AdaptadorBancosBD adaptador;
    //FIREBASE
    private AdaptadorBancosFirestore adaptadorBancosFirestore;
    private BancosAsinc bancosAsinc;
    private CollectionReference instanciaEdicion = FirebaseFirestore.getInstance().collection("bancos");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edicion_bancos);
        banco = new Bancos();
        bancos = ((Aplicacion) getApplication()).bancos;
        //usoBancos = new CasosUsoBancos(this, bancos);
        adaptador = ((Aplicacion) getApplication()).adaptador;
        bancosAsinc = ((Aplicacion) getApplication()).bancosAsinc;

        usoBancos = new CasosUsoBancos(this, bancosAsinc, adaptadorBancosFirestore);

        Query query = FirebaseFirestore.getInstance().collection("bancos").limit(50);
        FirestoreRecyclerOptions<Bancos> opciones = new FirestoreRecyclerOptions.Builder<Bancos>().setQuery(query, Bancos.class).build();
        adaptadorBancosFirestore = new AdaptadorBancosFirestore(opciones, this);

        Bundle extras = getIntent().getExtras();
        pos = extras.getInt("pos",0);
        _id = extras.getString("_id");
        String posEd = extras.getString("posEd");
        Log.d("TAG","pos edicion " + posEd );
        if (_id != null) {
            setTitle(getString(R.string.newplace));
            //banco = bancos.elemento(_id);
        } else banco = adaptadorBancosFirestore.getItem(pos); //banco = adaptador.bancosPosicion(pos); //banco = bancos.elemento(pos);
        Log.d("TAG EDA", "pos " + pos + " id " + _id);
        actualizaVistas();
    }

    public void actualizaVistas(){
        nombre = findViewById(R.id.nombre);
        //nombre.setText(banco.getNombre());

        direccion = findViewById(R.id.direccion);
        //direccion.setText(banco.getDireccion());

        telefono = findViewById(R.id.telefono);
        //telefono.setText(Long.toString(banco.getTelefono()));

        url = findViewById(R.id.url);
        //url.setText(banco.getUrl());

        horario = findViewById(R.id.horario);
        //horario.setText(banco.getHorario());

        hora = findViewById(R.id.hora);
        //hora.setText(banco.getHora());

        comentario = findViewById(R.id.comentario);
        //comentario.setText(banco.getComentario());

        tipo = findViewById(R.id.tipo);

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, TipoBancos.getNombres());
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tipo.setAdapter(adaptador);
        //tipo.setSelection(banco.getTipo().ordinal());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edicion_bancos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_guardar:
                banco.setNombre(nombre.getText().toString());
                banco.setTipo(TipoBancos.values()[tipo.getSelectedItemPosition()]);
                banco.setDireccion(direccion.getText().toString());
                banco.setTelefono(Long.parseLong(telefono.getText().toString()));
                banco.setUrl(url.getText().toString());
                banco.setComentario(comentario.getText().toString());
                banco.setHorario(horario.getText().toString());
                banco.setHora(hora.getText().toString());
                GeoPunto posicion = ((Aplicacion) getApplication()).posicionActual;
                if (!posicion.equals(GeoPunto.SIN_POSICION)) {
                    banco.setPosicion(posicion);
                }
                Log.d("TAG","posicion creada " + posicion);
                Log.d("TAG","nuevo lugar " + banco.toString());

                instanciaEdicion.add(banco).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("TAG", "DocumentSnapshot written with ID: " + documentReference.getId());
                        msnToast = Toast.makeText(getApplicationContext(), R.string.msjeba, Toast.LENGTH_LONG);
                        msnToast.show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        msnToast = Toast.makeText(getApplicationContext(), getString(R.string.msjerrorsave) + e.getMessage(), Toast.LENGTH_LONG);
                        msnToast.show();
                    }
                });

                //if (_id == -1) _id = adaptador.idPosicion(pos);
                //usoBancos.guardar(_id, banco);
                finish();
                return true;
            case R.id.accion_cancelar:
                if (_id != null) //bancos.borrar(_id);
                    Log.d("TAG","EDL cancelar id" + _id);
                    instanciaEdicion.document(_id).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            msnToast = Toast.makeText(getApplicationContext(), R.string.msncancel, Toast.LENGTH_LONG);
                            msnToast.show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            msnToast = Toast.makeText(getApplicationContext(), getString(R.string.errorcancel) + e.getMessage(), Toast.LENGTH_LONG);
                            msnToast.show();
                        }
                    });
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("tag","on stop ela ");
        /*if (_id !=-1 & nombre.getText().toString().isEmpty()){
            Log.d("tag","borrar"+banco.toString());
            bancos.borrar(_id);
        } else {
            Log.d("tag"," no borrar"+banco.toString());
        }*/
    }
}