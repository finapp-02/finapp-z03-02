package com.grupoz3.sitebank.Firebase;

import com.grupoz3.sitebank.modelo.Bancos;

public interface BancosAsinc {

    interface EscuchadorElemento {
        void onRespuesta(Bancos bancos);
    }
    interface EscuchadorTamaño {
        void onRespuesta(long tamaño);
    }
    void elemento(String id, EscuchadorElemento escuchador);
    void añade(Bancos bancos);
    String nuevo();
    void borrar(String id);
    void actualiza(String id, Bancos bancos);
    void tamaño(EscuchadorTamaño escuchador);
}
