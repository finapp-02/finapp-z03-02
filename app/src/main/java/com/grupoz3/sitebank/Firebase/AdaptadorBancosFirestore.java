package com.grupoz3.sitebank.Firebase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.grupoz3.sitebank.Aplicacion;
import com.grupoz3.sitebank.R;
import com.grupoz3.sitebank.modelo.Bancos;
import com.grupoz3.sitebank.modelo.GeoPunto;
import com.grupoz3.sitebank.presentacion.AdaptadorBancos;

public class AdaptadorBancosFirestore extends FirestoreRecyclerAdapter<Bancos, AdaptadorBancos.ViewHolder> {

    protected View.OnClickListener onClickListener;
    protected Context context;

    public AdaptadorBancosFirestore(FirestoreRecyclerOptions<Bancos> options, Context context) {
        super(options);
        this.context = context;
    }

    @Override
    public AdaptadorBancos.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.elemento_lista, parent, false);
        view.setOnClickListener(onClickListener);
        return new AdaptadorBancos.ViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(AdaptadorBancos.ViewHolder holder, int position, Bancos banco) {
        personalizaVista(holder, banco);
        holder.itemView.setOnClickListener(onClickListener);
        holder.itemView.setTag(new Integer(position));
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public String getKey(int pos) {
        return super.getSnapshots().getSnapshot(pos).getId();
    }

    public int getPos(String id) {
        int pos = 0;
        while (pos < getItemCount()) {
            if (getKey(pos).equals(id)) return pos;
            pos ++;
        }
        return -1;
    }

    // Personalizamos un ViewHolder a partir de un lugar
    public void personalizaVista(AdaptadorBancos.ViewHolder holder, Bancos banco) {
        holder.nombre.setText(banco.getNombre());
        holder.direccion.setText(banco.getDireccion());
        int id = R.drawable.otros;
        switch (banco.getTipo()) {
            case BANCOS: id = R.drawable.banco; break;
            case CAJERO: id = R.drawable.cajero; break;
            case CORRESPONSAL: id = R.drawable.corresponsal; break;
        }
        holder.foto.setImageResource(id);
        holder.foto.setScaleType(ImageView.ScaleType.FIT_END);
        holder.valoracion.setRating(banco.getValoracion());
        GeoPunto pos = ((Aplicacion) context.getApplicationContext()).posicionActual;
        if (pos.equals(GeoPunto.SIN_POSICION) || banco.getPosicion().equals(GeoPunto.SIN_POSICION)) {
            holder.distancia.setText("...Km");
        } else {
            int d = (int) pos.distancia(banco.getPosicion());
            if (d < 2000) {
                holder.distancia.setText(d + " m");
            } else {
                holder.distancia.setText(d / 1000 + " Km");
            }
        }
    }
}