package com.grupoz3.sitebank.Firebase;

import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.grupoz3.sitebank.modelo.Bancos;

public class BancosFirestore implements BancosAsinc {

    private CollectionReference bancos;

    public BancosFirestore() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        this.bancos = db.collection("bancos");
    }

    @Override
    public void elemento(String id, EscuchadorElemento escuchador) {
        bancos.document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>(){
            @Override
            public void onComplete(Task<DocumentSnapshot> task){
                if (task.isSuccessful()){
                    Bancos bancos = task.getResult().toObject(Bancos.class);
                    escuchador.onRespuesta(bancos);
                } else {
                    Log.d("BancosFi","Error leer bancos firestore ", task.getException());
                    escuchador.onRespuesta(null);
                }
            }
        });
    }

    @Override
    public void añade(Bancos banco) {
        bancos.document().set(banco);
    }

    @Override
    public String nuevo() {
        return  bancos.document().getId();
    }

    @Override
    public void borrar(String id) {
        bancos.document(id).delete();
    }

    @Override
    public void actualiza(String id, Bancos banco) {
        bancos.document(id).set(banco);
    }

    @Override
    public void tamaño(EscuchadorTamaño escuchador) {
        bancos.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>(){
            @Override
            public void onComplete(Task<QuerySnapshot> task){
                if(task.isSuccessful()){
                    escuchador.onRespuesta(task.getResult().size());
                } else{
                    Log.d("BancosFi","Error tamaño firestore bancos ",task.getException());
                    escuchador.onRespuesta(-1);
                }
            }
        });
    }
}