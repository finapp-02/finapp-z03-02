package com.grupoz3.sitebank.datos;

import com.grupoz3.sitebank.modelo.Bancos;

public interface RepositoriosBancos {

    Bancos elemento(int id); //Devuelve el elemento dado su id
    void agrega(Bancos bancos); //Añade el elemento indicado
    int nuevo(); //Añade un elemento en blanco y devuelve su id
    void borrar(int id); //Elimina el elemento con el id indicado
    int tamaño(); //Devuelve el número de elementos
    void actualizar(int id, Bancos bancos); //Reemplaza un elemento
}