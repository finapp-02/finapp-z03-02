package com.grupoz3.sitebank.datos;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.grupoz3.sitebank.Aplicacion;
import com.grupoz3.sitebank.R;
import com.grupoz3.sitebank.modelo.Bancos;
import com.grupoz3.sitebank.modelo.GeoPunto;
import com.grupoz3.sitebank.modelo.TipoBancos;

public class BancosBD extends SQLiteOpenHelper implements RepositoriosBancos {

    Context contexto;

    public BancosBD(Context contexto) {
        super(contexto, "bancos.db", null, 1);
        this.contexto = contexto;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //creamos la tabla de la base de datos
        db.execSQL("CREATE TABLE bancos (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "nombre TEXT, " +
                "direccion TEXT, " +
                "longitud REAL, " +
                "latitud REAL, " +
                "tipo INTEGER, " +
                "foto TEXT, " +
                "telefono BIGINT, " +
                "url TEXT, " +
                "comentario TEXT, " +
                "horario TEXT, " +
                "hora TEXT, " +
                "valoracion REAL)");

        //SENTENCIAS SQL PARA INSERTAR UN DATO EN LA TABLA lugares
        db.execSQL("INSERT INTO bancos VALUES (null, " +
                "'BBVA', " +
                "'Cl. 71B #100-11, Bogotá', -74.1418, 4.6862, " +
                TipoBancos.BANCOS.ordinal() + ", '', 13808175, " +
                "'https://www.bbva.com.co/', " +
                "'Muy lenta la atención al publico.', " +
                "'Lunes a Viernes', '9am-4pm', 2.0)");

        db.execSQL("INSERT INTO bancos VALUES (null, " +
                "'Bancolombia', " +
                "'Ac. 24 # 95 A 80, Fontibón, Bogotá, Cundinamarca', -74.1991, 4.6768, " +
                TipoBancos.CAJERO.ordinal() + ", '', 0, " +
                "'https://www.bancolombia.com/personas', " +
                "'Siempre está disponible y funcionando sin contratiempos 24/7.', " +
                "'Lunes a Domingo', '24 horas', 5.0)");

        db.execSQL("INSERT INTO bancos VALUES (null, " +
                "'Banco Bogota', " +
                "'Cra. 15 #7-9, Los Mártires, Pacho, Bogotá, Cundinamarca', -74.1544, 4.5989, " +
                TipoBancos.CORRESPONSAL.ordinal() + ", '', 0, " +
                "'https://www.bancodebogota.com/', " +
                "'Buen servicio.', " +
                "'Lunes a Domingo', '9am-7pm', 4.0)");

        db.execSQL("INSERT INTO bancos VALUES (null, " +
                "'Banco Davivienda', " +
                "'Cra. 103, Apartadó, Antioquia', -76.63364,7.88521, " +
                TipoBancos.BANCOS.ordinal() + ", '', 0, " +
                "'https://www.davivienda.com/', " +
                "'Buena atención, pocos cajeros para atender mínimo 3', " +
                "'Lunes a Domingo', '8am-4pm', 4.0)");

        db.execSQL("INSERT INTO bancos VALUES (null, " +
                "'Banco Caja Social', " +
                "'Cl. 99C N° 100-82  Plaza Del Rio, Apartadó, Antioquia', -76.6334,7.8837, " +
                TipoBancos.CAJERO.ordinal() + ", '', 018000910038, " +
                "'https://www.bancocajasocial.com/', " +
                "'Se llena mucho', " +
                "'Lunes a Domingo', '24 horas', 2.0)");

        db.execSQL("INSERT INTO bancos VALUES (null, " +
                "'Cajero Automático Red Aval', " +
                "'Cl. 100 # 95 - 02, Apartadó, Antioquia', -76.6375,7.88004, " +
                TipoBancos.CAJERO.ordinal() + ", '', 17451616, " +
                "'https://www.redaval.com.co/', " +
                "'Siempre está disponible 24/7', " +
                "'Lunes a Domingo', '24 horas', 4.5)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public Bancos elemento(int id) {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM bancos WHERE _id = " + id, null);
        try {
            if (cursor.moveToNext()) {
                return extraeBancos(cursor);
            } else {
                throw new SQLException(contexto.getString(R.string.erroracceder) + id);
            }
        } catch (Exception e) {
            Log.d("TAG", "Error elemento bancos db exec " + e.getMessage());
            throw e;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void agrega(Bancos bancos) {

    }

    @Override
    public int nuevo() {
        int _id = -1;
        Bancos banco = new Bancos();
        getWritableDatabase().execSQL("INSERT INTO bancos (nombre, " +
                "direccion, longitud, latitud, tipo, foto, telefono, url, " +
                "comentario, horario, hora, valoracion) VALUES ('', '', " +
                banco.getPosicion().getLongitud() + "," +
                banco.getPosicion().getLatitud() + ", " +
                banco.getTipo().ordinal() +
                ", '', 0, '', '', '', '', 0)");
        Cursor c = getReadableDatabase().rawQuery("SELECT _id FROM bancos WHERE tipo = " + banco.getTipo().ordinal(), null);
        if (c.moveToNext()) _id = c.getInt(0);
        c.close();
        return _id;
    }

    @Override
    public void borrar(int id) {
        getWritableDatabase().execSQL("DELETE FROM bancos WHERE _id = " + id);
    }

    @Override
    public int tamaño() {
        return 0;
    }

    @Override
    public void actualizar(int id, Bancos bancos) {
        getWritableDatabase().execSQL("UPDATE bancos SET" +
                " nombre = '" + bancos.getNombre() +
                "', direccion = '" + bancos.getDireccion() +
                "', longitud = " + bancos.getPosicion().getLongitud() +
                " , latitud = " + bancos.getPosicion().getLatitud() +
                " , tipo = " + bancos.getTipo().ordinal() +
                " , foto = '" + bancos.getFoto() +
                "', telefono = " + bancos.getTelefono() +
                " , url = '" + bancos.getUrl() +
                "', comentario = '" + bancos.getComentario() +
                "', horario = '" + bancos.getHorario() +
                "', hora = '" + bancos.getHora() +
                "', valoracion = " + bancos.getValoracion() +
                " WHERE _id = " + id);
    }

    public static Bancos extraeBancos(Cursor cursor) {
        Bancos bancos = new Bancos();
        bancos.setNombre(cursor.getString(1));
        bancos.setDireccion(cursor.getString(2));
        bancos.setPosicion(new GeoPunto(cursor.getDouble(3), cursor.getDouble(4)));
        bancos.setTipo(TipoBancos.values()[cursor.getInt(5)]);
        bancos.setFoto(cursor.getString(6));
        bancos.setTelefono(cursor.getLong(7));
        bancos.setUrl(cursor.getString(8));
        bancos.setComentario(cursor.getString(9));
        bancos.setHorario(cursor.getString(10));
        bancos.setHora(cursor.getString(11));
        bancos.setValoracion(cursor.getFloat(12));
        return bancos;
    }

    public Cursor extraeCursor() {
        //String consulta = "SELECT * FROM bancos";
        //String consulta = "SELECT * FROM bancos WHERE valoracion > 1.0 ORDER BY nombre DESC";
        String consulta, max;
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(contexto);
        max = pref.getString("maximo", "12");

        switch (pref.getString("orden", "0")) {
            case "0":
                consulta = "SELECT * FROM bancos";
                break;
            case "1":
                consulta = "SELECT * FROM bancos ORDER BY valoracion DESC";
                break;
            case "2":
                double lon = ((Aplicacion) contexto.getApplicationContext()).posicionActual.getLongitud();
                double lat = ((Aplicacion) contexto.getApplicationContext()).posicionActual.getLatitud();
                consulta = "SELECT * FROM bancos ORDER BY " +
                        " (" + lon + "-longitud) * (" + lon + "-longitud) + " +
                        " (" + lat + "-latitud ) * (" + lat + "-latitud )";
                break;
            case "3":
                consulta = "SELECT * FROM bancos WHERE tipo = 1 ";
                break;
            case "4":
                consulta = "SELECT * FROM bancos WHERE tipo = 2";
                break;
            default:
                consulta = "SELECT * FROM bancos WHERE tipo = 3;";
                break;
        }
            /*
           switch (pref.getString("tipobanco","0")){
            case "0":
                consulta = "SELECT * FROM bancos";
                break;
            case "1":
                consulta = "SELECT * FROM bancos WHERE tipo = 1 ";
                break;
            case "2":
                consulta = "SELECT * FROM bancos WHERE tipo = 2";
                break;
            default:
                consulta = "SELECT * FROM bancos WHERE tipo = 3;";
                break;
        }
        */
        consulta += " LIMIT " + max;
        Log.d("TAG","consulta sql "+ consulta);
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery(consulta, null);
    }
}