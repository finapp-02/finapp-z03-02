package com.grupoz3.sitebank.datos;

import com.grupoz3.sitebank.modelo.Bancos;
import com.grupoz3.sitebank.modelo.TipoBancos;
import java.util.ArrayList;
import java.util.List;

public class BancosLista implements RepositoriosBancos {

    protected List<Bancos> listaBancos;

    //Constructor de la clase
    public BancosLista() {
        listaBancos = new ArrayList<Bancos>();
        agregarEjemplos();
    }

    @Override
    public Bancos elemento(int id) {
        return listaBancos.get(id);
    }

    @Override
    public void agrega(Bancos bancos) {
        listaBancos.add(bancos);
    }

    @Override
    public int nuevo(){
        Bancos bancos = new Bancos();
        listaBancos.add(bancos);
        return listaBancos.size()-1;
    }

    @Override
    public void borrar (int id){
        listaBancos.remove(id);
    }

    @Override
    public int tamaño(){
        return listaBancos.size();
    }

    @Override
    public void actualizar(int id, Bancos bancos){
        listaBancos.set(id,bancos);
    }

    public void agregarEjemplos(){
        agrega(new Bancos("BBVA", "Cl. 82 #11-75, Bogotá D.C.", -74.0557, 4.6666, TipoBancos.BANCOS, 6344000L, "https://www.bbva.com/es/us/" ,"Lunes a Viernes", "7am-7pm","Uno de los mejores bancos",5));
        agrega(new Bancos("Bancolombia", "Cra. 15 # 79-05", -74.0592, 4.6655, TipoBancos.CAJERO, 6344000L, "https://www.bancolombia.com/personas", "Lunes a Viernes", "9am-6pm", "buen servicio",5));
        agrega(new Bancos("PagaFacil", "Cl. 40 #80d-69, Medellín, Laureles", -75.6188, 6.2468, TipoBancos.CORRESPONSAL, 6344000L, "https://www.pagafacil.com.co/", "Lunes a Viernes", "8am-4pm","aqui atracan",5));
    }
}