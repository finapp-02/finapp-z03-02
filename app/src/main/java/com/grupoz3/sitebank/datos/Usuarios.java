package com.grupoz3.sitebank.datos;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.grupoz3.sitebank.modelo.Usuario;

public class Usuarios {

    public static void guardarUsuario(final FirebaseUser user) {
        Usuario usuario = new Usuario(user.getDisplayName(), user.getEmail());
        FirebaseFirestore baseDatos = FirebaseFirestore.getInstance();
        baseDatos.collection("usuarios").document(user.getUid()).set(usuario);
    }
}