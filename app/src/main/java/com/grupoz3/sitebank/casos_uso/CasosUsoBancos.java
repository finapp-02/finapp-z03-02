package com.grupoz3.sitebank.casos_uso;

import static com.grupoz3.sitebank.casos_uso.CasosUsoLocalizacion.solicitarPermiso;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.core.content.ContextCompat;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.grupoz3.sitebank.Aplicacion;
import com.grupoz3.sitebank.Firebase.AdaptadorBancosFirestore;
import com.grupoz3.sitebank.Firebase.BancosAsinc;
import com.grupoz3.sitebank.R;
import com.grupoz3.sitebank.datos.BancosBD;
import com.grupoz3.sitebank.datos.RepositoriosBancos;
import com.grupoz3.sitebank.modelo.Bancos;
import com.grupoz3.sitebank.modelo.GeoPunto;
import com.grupoz3.sitebank.presentacion.AdaptadorBancosBD;
import com.grupoz3.sitebank.presentacion.EdicionBancosActivity;
import com.grupoz3.sitebank.presentacion.VistaBancosActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class CasosUsoBancos {

    private Activity actividad;
    //private RepositoriosBancos bancos;
    // PERMISO GALERIA READ_EXTERNAL_STORAGE
    private static final int SOLICITUD_PERMISO_LECTURA = 0;
    //base de datos sqlite
    private BancosBD bancos;
    private AdaptadorBancosBD adaptador;

    //FIREBASE
    private AdaptadorBancosFirestore adaptadorBancosFirestore;
    public BancosAsinc bancosAsinc;
    private CollectionReference instanciaColeccion = FirebaseFirestore.getInstance().collection("bancos");

    //constructor de la clase
    public CasosUsoBancos(Activity actividad, BancosAsinc bancos, AdaptadorBancosFirestore adaptador) {
        this.actividad = actividad;
        this.bancosAsinc = bancos;
        this.adaptadorBancosFirestore = adaptador;
        Query query = FirebaseFirestore.getInstance().collection("bancos").limit(50);
        FirestoreRecyclerOptions<Bancos> opciones = new FirestoreRecyclerOptions.Builder<Bancos>().setQuery(query, Bancos.class).build();
        adaptadorBancosFirestore = new AdaptadorBancosFirestore(opciones, actividad.getApplicationContext());
    }

    //Operación básicas
    public void mostrar (String pos){
        Intent mostrar = new Intent(actividad, VistaBancosActivity.class);
        mostrar.putExtra("pos", pos);
        actividad.startActivity(mostrar);
    }

    public void borrar(final String id) {
        //Log.d("usosBancos", " tamaño " + adaptador.getItemId(id));
        //Log.d("usosBancos"," tamaño " + adaptadorBancosFirestore.getPos(id));
        new AlertDialog.Builder(actividad)
                .setTitle(R.string.borradodebanco)
                .setMessage(R.string.pregunta_borrado)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        /*
                        bancos.borrar(id);
                        adaptador = ((Aplicacion) actividad.getApplicationContext()).adaptador;
                        adaptador.setCursor(bancos.extraeCursor());
                        adaptador.notifyDataSetChanged();
                        actividad.finish();*/
                        instanciaColeccion.document(id).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {
                                Toast.makeText(actividad.getApplicationContext(), R.string.msnelimlugar, Toast.LENGTH_LONG).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(Exception e) {
                                Toast.makeText(actividad.getApplicationContext(), actividad.getString(R.string.errorelimlugfir) + e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                        adaptadorBancosFirestore.notifyDataSetChanged();
                        actividad.finish();
                    }
                })
                .setNegativeButton(R.string.cancelar, null)
                .show();
    }

    public void eliminar_foto(final int id, ImageView foto) {
        new AlertDialog.Builder(actividad)
                .setTitle(R.string.elimfoto)
                .setIcon(R.mipmap.icono_app)
                .setMessage(R.string.pregelimfoto)
                .setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Toast.makeText(actividad.getApplicationContext(), R.string.msnfotoelim, Toast.LENGTH_LONG).show();
                        ponerFoto(id,"", foto);
                    }
                })
                .setNegativeButton("NO", null)
                .show();
    }

    public void editar(int pos, int codigoSolicitud){
        Intent intent_ed_banco = new Intent(actividad, EdicionBancosActivity.class);
        intent_ed_banco.putExtra("pos", pos);
        actividad.startActivityForResult(intent_ed_banco, codigoSolicitud);
    }

    public void guardar(String id, Bancos nuevoBanco){
        //bancos.actualizar(id, nuevoBanco);
        bancosAsinc.actualiza(id, nuevoBanco);
        //adaptador = ((Aplicacion) actividad.getApplicationContext()).adaptador;
        //adaptador.setCursor(bancos.extraeCursor());
        adaptadorBancosFirestore.notifyDataSetChanged();
        //adaptador.notifyDataSetChanged();
    }

    public void nuevo() {
        /*
        int id = bancos.nuevo();
        GeoPunto posicion = ((Aplicacion) actividad.getApplication()).posicionActual;
        if (!posicion.equals(GeoPunto.SIN_POSICION)) {
            Bancos banco = bancos.elemento(id);
            banco.setPosicion(posicion);
            bancos.actualizar(id, banco);
        }
        Intent nuevo_banco = new Intent(actividad, EdicionBancosActivity.class);
        nuevo_banco.putExtra("_id", id);
        actividad.startActivity(nuevo_banco);*/
        String id = bancosAsinc.nuevo();
        Intent nuevo_banco = new Intent(actividad, EdicionBancosActivity.class);
        nuevo_banco.putExtra("_id", id);
        actividad.startActivity(nuevo_banco);
    }

    //INTENCIONES
    public void compartir(Bancos bancos) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT,actividad.getString(R.string.msjshare)
                + bancos.getNombre() + "\n" + bancos.getDireccion() + "\n" +
                bancos.getHorario() + " - " + bancos.getHora() + "\n" + bancos.getTelefono());
        actividad.startActivity(i);
    }

    public void llamarTelefono(Bancos bancos) {
        actividad.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("Tel: " + bancos.getTelefono())));
    }

    public void verWeb(Bancos bancos) {
        actividad.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(bancos.getUrl())));
    }

    public final void verMapa(Bancos bancos) {
        double lat = bancos.getPosicion().getLatitud();
        double lon = bancos.getPosicion().getLongitud();
        Uri uri = bancos.getPosicion() != GeoPunto.SIN_POSICION ? Uri.parse("geo:" + lat + ',' +
                lon + "?z=18&q=" + Uri.encode(bancos.getDireccion()))
                :Uri.parse("geo:0,0?q=" + Uri.encode(bancos.getDireccion()));
        Log.d("tag casos uso lugar","vermapa " + uri + " " +
                Uri.encode(bancos.getDireccion())+ "\n" + bancos.getPosicion() + " geopto " + GeoPunto.SIN_POSICION);
        actividad.startActivity(new Intent("android.intent.action.VIEW", uri));
    }

    //FOTOGRAFÍAS
    public void ponerDeGaleria(int codigoSolicitud) {
        String action;
        if (Build.VERSION.SDK_INT >= 19) {
            action = Intent.ACTION_OPEN_DOCUMENT;
        } else  {
            action = Intent.ACTION_PICK;
        }
        Intent i = new Intent(action, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("image/*");
        /*
        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        i.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        */
        actividad.startActivityForResult(i, codigoSolicitud);
    }

    public void ponerFoto(int pos, String uri, ImageView imageView) {
        //Bancos banco = bancos.elemento(pos);
        Bancos banco = adaptador.bancosPosicion(pos);
        banco.setFoto(uri);
        visualizarFoto(banco, imageView);
        actualizaPosBancos(pos, banco);
    }

    public void visualizarFoto(Bancos banco, ImageView imageView) {
        if (banco.getFoto() != null && !banco.getFoto().isEmpty()) {
            //GESTION DEL PERMISO DE LECTURA DEL ALMACENAMIENTO
            if (ContextCompat.checkSelfPermission(actividad, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                imageView.setImageURI(Uri.parse(banco.getFoto()));
                //imageView.setImageBitmap(reduceBitmap(actividad, banco.getFoto(), 1024, 1024));
            } else {
                imageView.setImageBitmap(null);
                solicitarPermiso(Manifest.permission.READ_EXTERNAL_STORAGE, actividad.getString(R.string.sinpermisolectura), SOLICITUD_PERMISO_LECTURA, actividad);
            }
        } else {
            imageView.setImageBitmap(null);
        }
    }

    private Bitmap reduceBitmap(Context contexto, String uri, int maxAncho, int maxAlto) {
        try {
            InputStream input = null;
            Uri u = Uri.parse(uri);
            if (u.getScheme().equals("http") || u.getScheme().equals("https")) {
                input = new URL(uri).openStream();
            } else {
                input = contexto.getContentResolver().openInputStream(u);
            }
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            options.inSampleSize = (int) Math.max(
                    Math.ceil(options.outWidth / maxAncho),
                    Math.ceil(options.outHeight / maxAlto));
            Log.d("TAG cul","tamaño foto " + (int) Math.max(
                    options.outWidth / maxAncho,
                    options.outHeight / maxAlto));
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(input, null, options);
        } catch (FileNotFoundException e) {
            Toast.makeText(contexto, R.string.ficheroimg+ e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            Toast.makeText(contexto, R.string.errorimg + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return null;
        }
    }

    public Uri tomarFoto(int codidoSolicitud) {
        try {
            Uri uriUltimaFoto;
            File file = File.createTempFile("img_" + (System.currentTimeMillis()/ 1000), ".jpg",
                    actividad.getExternalFilesDir(Environment.DIRECTORY_PICTURES));
            //PERMISO DE LECTURA AL ABRIR LA CAMARA
            if (ContextCompat.checkSelfPermission(actividad, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                solicitarPermiso(Manifest.permission.READ_EXTERNAL_STORAGE, actividad.getString(R.string.sinpermisocam), SOLICITUD_PERMISO_LECTURA, actividad);
                uriUltimaFoto = Uri.parse(String.valueOf(R.mipmap.icono_app));
            } else if (Build.VERSION.SDK_INT >= 24) {
                uriUltimaFoto = FileProvider.getUriForFile(actividad,"misiontic.uis.sitebank.fileProvider", file);
            } else {
                uriUltimaFoto = Uri.fromFile(file);
            }
            Intent intentoTomarFoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intentoTomarFoto.putExtra(MediaStore.EXTRA_OUTPUT, uriUltimaFoto);
            actividad.startActivityForResult(intentoTomarFoto, codidoSolicitud);
            return uriUltimaFoto;
        } catch (IOException ex) {
            Toast.makeText(actividad, R.string.errorcrearfichero, Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    public void actualizaPosBancos(int pos, Bancos banco) {
        String id = adaptadorBancosFirestore.getKey(pos);
        guardar(id, banco);
    }
}