package com.grupoz3.sitebank.modelo;

import com.grupoz3.sitebank.R;

public enum TipoBancos {
    OTROS("Otros", R.drawable.otros),
    BANCOS("Bancos",R.drawable.banco),
    CAJERO("Cajeros",R.drawable.cajero),
    CORRESPONSAL("Corresponsal",R.drawable.corresponsal);

    private final String texto;
    private final int recurso;

    //CONSTRUCTOR
    TipoBancos(String texto, int recurso) {
        this.texto = texto;
        this.recurso = recurso;
    }

    //GET DE LOS DOS ATRIBUTOS DEL ENUM
    public String getTexto(){
        return texto;
    }

    public int getRecurso(){
        return recurso;
    }

    public static String [] getNombres(){
        String[] resultado = new String[TipoBancos.values().length];
        for (TipoBancos tipo : TipoBancos.values()){
            resultado[tipo.ordinal()] = tipo.texto;
        }
        return resultado;
    }
}