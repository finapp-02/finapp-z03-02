package com.grupoz3.sitebank.modelo;

public class Bancos {

    //ATRIBUTOS DE LA CLASE BANCOS
    private String nombre;
    private String direccion;
    private GeoPunto posicion;
    private TipoBancos tipo;
    private String foto;
    private Long telefono;
    private String url;
    private String comentario;
    private String horario;
    private String hora;
    private float valoracion;

    //Constructor
    public Bancos(String nombre, String direccion, double longitud, double latitud,
                  TipoBancos tipo, Long telefono, String url, String horario, String hora,
                  String comentario, float valoracion) {
        posicion = new GeoPunto(longitud,latitud);
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.url = url;
        this.horario = horario;
        this.hora = hora;
        this.comentario = comentario;
        this.valoracion = valoracion;
        this.tipo = tipo;
    }

    public Bancos() {
        posicion = new GeoPunto(0.0,0.0);
        tipo = TipoBancos.OTROS;
    }

    //Metodos Getter y Setter
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public GeoPunto getPosicion() {
        return posicion;
    }

    public void setPosicion(GeoPunto posicion) {
        this.posicion = posicion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Long getTelefono() {
        return telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public float getValoracion() {
        return valoracion;
    }

    public void setValoracion(float valoracion) {
        this.valoracion = valoracion;
    }

    public TipoBancos getTipo() {
        return tipo;
    }

    public void setTipo(TipoBancos tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Bancos{" +
                "nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                ", posicion=" + posicion +
                ", foto='" + foto + '\'' +
                ", telefono=" + telefono +
                ", url='" + url + '\'' +
                ", comentario='" + comentario + '\'' +
                ", horario='" + horario + '\'' +
                ", hora='" + hora + '\'' +
                ", valoracion=" + valoracion +
                ", tipo=" + tipo +
                '}';
    }
}